module.exports = {
  root: true,

  env: {
    node: true,
    browser: true
  },

  extends: ["eslint:recommended", "plugin:vue/recommended"],

  parserOptions: {
    parser: "babel-eslint"
  },

  rules: {
    "vue/component-name-in-template-casing": ["error", "PascalCase"],
    "no-console": "off",
    "no-debugger": "off"
  },

  overrides: [
    {
      files: [
        "**/__tests__/*.{j,t}s?(x)",
        "**/tests/unit/**/*.spec.{j,t}s?(x)"
      ],
      env: {
        jest: true
      }
    }
  ]
};
