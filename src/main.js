import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import vuetify from "@/plugins/vuetify";
import CoVid from "@/components/CoVid";

Vue.config.productionTip = false;

Vue.component("CoVid", CoVid);
new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount("#app");
