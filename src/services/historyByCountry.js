import allData from "../../data/json/time_series_covid19_all_global.json";

const historyByCountry = ({ country }) => {
  return new Promise((resolve, reject) => {
    if (allData[country]) {
      resolve(allData[country].history);
    } else {
      reject("Pais no encontrado");
    }
  });
};

export default historyByCountry;
