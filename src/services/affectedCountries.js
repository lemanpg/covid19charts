import allData from "../../data/json/time_series_covid19_all_global.json";

const affectedCountries = () => {
  return new Promise(resolve => {
    resolve(Object.keys(allData));
  });
};

const onlyUnique = (value, index, self) =>
  value && self.indexOf(value) === index;

export const getContinentCountries = continent =>
  Object.values(allData)
    .filter(countryData => countryData.continent === continent)
    .map(countryData => countryData["Country/Region"]);

export const continents = Object.values(allData)
  .map(({ continent }) => continent)
  .filter(onlyUnique);

export default affectedCountries;
