import allData from "../../data/json/time_series_covid19_all_global.json";

const historyByCountries = ({ countries }) => {
  return new Promise(resolve => {
    resolve(countries.map(country => allData[country]));
  });
};

export default historyByCountries;
