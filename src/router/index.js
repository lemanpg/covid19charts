import Vue from "vue";
import VueRouter from "vue-router";
import History from "@/views/History.vue";
import Comparator from "@/views/Comparator.vue";
import NotFound from "@/views/NotFound.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/history",
    name: "History",
    component: History
  },
  {
    path: "/comparator",
    name: "Comparator",
    component: Comparator
  },
  {
    path: "/",
    name: "Home",
    redirect: { name: "Comparator" }
  },
  {
    path: "*",
    name: "404",
    component: NotFound
  }
];

const router = new VueRouter({
  mode: "hash",
  base: process.env.BASE_URL,
  routes
});

export default router;
