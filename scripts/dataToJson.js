var csvtojson = require("csvtojson");
var fs = require("fs");
var path = require("path");
var _ = require("lodash");
var countriesByContinents = require("../data/json/countriesByContinent.json");

const getCountryContinent = country =>
  countriesByContinents.find(
    countryContinent => countryContinent.country === country
  )
    ? countriesByContinents.find(
        countryContinent => countryContinent.country === country
      ).continent
    : null;

const csvFiles = ["confirmed", "deaths", "recovered"];

const inPath = fileName =>
  path.join(
    __dirname,
    `../data/csv/time_series_covid19_${fileName}_global.csv`
  );
const outPath = fileName =>
  path.join(
    __dirname,
    `../data/json/time_series_covid19_${fileName}_global.json`
  );

const sanitizeData = (json, dateType) => {
  return json.map(item => {
    return Object.keys(item).reduce((parsed, key) => {
      if (!isNaN(new Date(key))) {
        const date = new Date(key);
        if (date.getHours() >= 12) {
          date.setDate(date.getDate() + 1);
        }
        date.setHours(12, 0, 0, 0);
        return {
          ...parsed,
          history: {
            ...(parsed.history ? { ...parsed.history } : {}),
            [date.toISOString().split("T")[0]]: {
              date: date.toISOString().split("T")[0],
              [dateType]: item[key]
            }
          }
        };
      } else {
        return {
          ...parsed,
          [key]: item[key]
        };
      }
    }, {});
  });
};

const promises = csvFiles.map(fileName => {
  const converter = csvtojson({
    noheader: false,
    trim: true
  });
  return converter.fromFile(inPath(fileName)).then(json => {
    return sanitizeData(json, fileName);
  });
});

function mergeCountryByData(arrais) {
  return arrais
    .reduce((countries, array) => {
      return [
        ...countries,
        ...array.reduce((withOutProvinces, countryData) => {
          const country = {
            continent: getCountryContinent(countryData["Country/Region"]),
            ...countryData
          };
          if (country["Province/State"]) {
            const itemCountry = withOutProvinces.find(
              c => c["Country/Region"] === country["Country/Region"]
            );
            if (itemCountry) {
              return [
                ...withOutProvinces.filter(item => item !== itemCountry),
                {
                  ...country,
                  history: Object.keys(country.history).reduce(
                    (byDate, date) => {
                      return {
                        ...byDate,
                        [date]: {
                          date: [date],
                          ...Object.keys(country.history[date]).reduce(
                            (historyByDay, key) => {
                              if (key !== "date") {
                                return {
                                  ...country.history[date],
                                  [key]:
                                    parseInt(country.history[date][key]) +
                                    parseInt(itemCountry.history[date][key])
                                };
                              } else {
                                return country.history[date];
                              }
                            },
                            {}
                          )
                        }
                      };
                    },
                    {}
                  )
                }
              ];
            } else {
              delete country["Province/State"];
              return [...withOutProvinces, country];
            }
          } else {
            return [...withOutProvinces, country];
          }
        }, [])
      ];
    }, [])
    .reduce((byCountry, country) => {
      return {
        ...byCountry,
        [country["Country/Region"]]: {
          ...(byCountry[country["Country/Region"]]
            ? _.merge(byCountry[country["Country/Region"]], country)
            : country)
        }
      };
    }, {});
}

Promise.all(promises).then(files => {
  fs.writeFileSync(
    outPath("all"),
    JSON.stringify(mergeCountryByData(files), null, 2),
    "utf8",
    function(err) {
      console.err(err);
    }
  );
});
